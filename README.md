JsonMapper
===
**Library for mapping json data to objects using constructor**

![Downloads](https://img.shields.io/packagist/dt/nstwf/json-mapper?style=flat-square)
![Pipeline status](https://img.shields.io/gitlab/pipeline-status/nstwf/json-mapper?branch=main&style=flat-square)
![Code coverage](https://img.shields.io/gitlab/coverage/nstwf/json-mapper/main?color=d&logo=fff&logoColor=aggsa&style=flat-square)
![Tag](https://img.shields.io/gitlab/v/tag/nstwf/json-mapper?style=flat-square)
![PHP](https://img.shields.io/packagist/php-v/nstwf/json-mapper?style=flat-square)




Contents
---

- [Installation](#installation)
- [Usage](#usage)
- [Supported types](#supported-types)
- [Inspectors](#inspectors)
    - [Notes](#notes)
    - [Typed properties](#typed-properties)
    - [Property `@var`  annotations](#property-var--annotations)
    - [Constructor typed parameters](#constructor-typed-parameters)
    - [Constructor `@param` annotations](#constructor-param-annotations)
    - [Cache](#cache)
- [Customize](#customize)
    - [Types mapping](#customize-types-mapping)
    - [Inspectors](#customize-inspectors)
- [Based on](#based-on)

## Installation

```bash
composer require nstwf/json-mapper
```

## Usage

```php
$factory = new \Nstwf\JsonMapper\JsonMapper\JsonMapperFactory();
$jsonMapper = $factory->create();

$data = (object)[
    "id"      => "123e4567-e89b-12d3-a456-426614174000",
    "roles"   => [1, 3, 7],
    "profile" => [
        "name" => "Alex",
        "age"  => 19,
    ]
];

$jsonMapper->mapObject($data, User::class);
```

## Supported types:

* Scalar types (int, float, bool, string, array, mixed)
* Enums
* Classes
* Array of scalar types, enums and classes
* Nullable types

## Inspectors

Here describe all supported inspectors. You can [customize them](#customize-inspectors), if you do not use some features describe there - to optimize it

## Notes:

- To declare array of any type (especially for class and enum) - use phpDoc
- If class in phpDoc does not contain namespace - try to discover namespace from the use statement if it exists
- Constructor parameter must use same name as property
- Union type is chosen by equal comparison with the data type:
    - For scalar only: if no one of described types is not suitable - value will be cast to first type
- Union types declare from left to right.
    - **Be careful**: enum with backed type can be mapped as scalar type value, if it uses together in union. Preferred types should come first

### Typed properties

```php
$inspector = (new \Nstwf\JsonMapper\Inspector\InspectorBuilder())
                ->withTypesProperties()
                ->build();
```

```php
class Example {
    private int $scalar;
    private int|string $union;
    private ?int $nullable;
    private User $class;
}
```

### Property `@var`  annotations

```php
$inspector = (new \Nstwf\JsonMapper\Inspector\InspectorBuilder())
                ->withPropertyAnnotations()
                ->build();
```

```php
use App\Models\User;

class Example {
    /** @var int */
    private $scalar;
    /** @var int|string */
    private $union;
    /** @var int[] */
    private $array;
    /** @var int|null */
    private $nullable;
    /** @var User */
    private $class;
    /** @var \App\Models\Role */
    private $classWithNamespace;
}
```

### Constructor typed parameters

```php
$inspector = (new \Nstwf\JsonMapper\Inspector\InspectorBuilder())
                ->withConstructorParameters()
                ->build();
```

```php
class Example {
    public function __construct(
        int $scalar,
        int|string $union,
        ?int $nullable,
        User $class
    ) {
       $this->scalar = $scalar;
       /** more */
    }
}
```

### Constructor `@param` annotations

```php
$inspector = (new \Nstwf\JsonMapper\Inspector\InspectorBuilder())
                ->withConstructorAnnotations()
                ->build();
```

```php
use App\Models\User;

class Example {

    /**
    * @param int                $scalar
    * @param int|string         $union
    * @param int[]              $array
    * @param int|null           $nullable
    * @param User               $class
    * @param \App\Models\Role   $classWithNamespace
     */
    public function __construct(/*parameters*/) {}
}
```

### Property `CustomName`  attribute

```php
$inspector = (new \Nstwf\JsonMapper\Inspector\InspectorBuilder())
                ->withCustomNameAttribute()
                ->build();
```

```php
class Example {
    #[\Nstwf\JsonMapper\Attribute\CustomName('custom_name')]
    private $property;
}
```

## Cache

All inspectors can receive cache. Default cache for inspectors - array. You can use self implementation, using: `Psr\SimpleCache\CacheInterface`

```php
$inspectorBuilder = new \Nstwf\JsonMapper\Inspector\InspectorBuilder();

$inspector = $inspectorBuilder
               ->withCache($redisCache)
               ->withTypesProperties()
               ->withPropertyAnnotations()
               ->build();
```

## Customize

Use native builders to customize your json mapper for your needs

```php
// Customize inspectors
$inspectorBuilder = new \Nstwf\JsonMapper\Inspector\InspectorBuilder();
$inspector = $inspectorBuilder
               ->withTypesProperties()
               ->withPropertyAnnotations()
               ->build();
                 
// Add own types to registry
$registry = new \Nstwf\JsonMapper\Registry\ClassFactoryRegistry();
$registry->add(Uuid::class, fn(string $value) => new Uuid($value));

// Use object mapper builder
$propertyMapperBuilder = new \Nstwf\JsonMapper\Property\Mapper\PropertyMapperBuilder();
$propertyMapper = $propertyMapperBuilder
                    ->withClassFactoryRegistry($registry)
                    ->build();
                    
$objectMapperBuilder = new \Nstwf\JsonMapper\Object\Mapper\ObjectMapperBuilder();
$objectMapper = $objectMapperBuilder
                    ->withPropertyMapper($propertyMapper)
                    ->build();

// Create JsonMapper
$factory = new \Nstwf\JsonMapper\JsonMapper\JsonMapperFactory();

$jsonMapper = $factory->create($inspector, $objectMapper);
```

### Customize types mapping

You can customize mapping for some classes. For example: _json contains uuid as string, but in the object it defined as Uuid class_. Just add class with custom mapping

```json
{
  "id": "123e4567-e89b-12d3-a456-426614174000"
}
```

```php
class User {
    private Uuid $id;
}
```

```php
$registry = new \Nstwf\JsonMapper\Registry\ClassFactoryRegistry();

$registry->add(
    Uuid::class, fn(string $value) => new Uuid($value)
);
```

### Customize inspectors

You can specify type discover using builder. All available inspectors defined [here](#inspectors).

```php
// Example: if you do not use constructors
$builder = new \Nstwf\JsonMapper\Inspector\InspectorBuilder();
$builder->withTypesProperties()
        ->withPropertyAnnotations()
        ->build();
```

## Based on:

- **https://github.com/cweiske/jsonmapper**
- **https://github.com/JsonMapper/JsonMapper**