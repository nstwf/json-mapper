<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Attribute;


#[\Attribute(\Attribute::TARGET_PROPERTY)]
final class CustomName
{
    public function __construct(
        private string $customName
    ) {
    }

    public function getCustomName(): string
    {
        return $this->customName;
    }
}