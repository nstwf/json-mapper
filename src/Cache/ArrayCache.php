<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Cache;


use Psr\SimpleCache\CacheInterface;


final class ArrayCache implements CacheInterface
{
    /** @var array<string, iterable> */
    private array $cache = [];

    public function get(string $key, mixed $default = null): mixed
    {
        return $this->cache[$key] ?? $default;
    }

    public function set(string $key, mixed $value, \DateInterval|int|null $ttl = null): bool
    {
        $this->cache[$key] = $value;

        return true;
    }

    public function delete(string $key): bool
    {
        unset($this->cache[$key]);

        return true;
    }

    public function clear(): bool
    {
        $this->cache = [];

        return true;
    }

    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
        $keys = (array)$keys;

        $values = \array_map(
            fn(mixed $key) => $this->cache[$key] ?? $default,
            $keys
        );

        return array_combine($keys, $values);
    }

    public function setMultiple(iterable $values, \DateInterval|int|null $ttl = null): bool
    {
        $this->cache = \array_merge($this->cache, (array)$values);

        return true;
    }

    public function deleteMultiple(iterable $keys): bool
    {
        foreach ($keys as $key) {
            $this->delete($key);
        }

        return true;
    }

    public function has(string $key): bool
    {
        return \array_key_exists($key, $this->cache);
    }
}