<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Data;


use stdClass;


final class DataMap
{
    private array $data = [];

    public static function create(stdClass|array $data): self
    {
        $self = new self();

        foreach ($data as $key => $value) {
            $self->add($key, $value);
        }

        return $self;
    }

    public function add(int|string $key, mixed $value): void
    {
        $this->data[$key] = $value;
    }

    public function get(string $key): mixed
    {
        return $this->data[$key];
    }

    public function has(string $key): bool
    {
        return array_key_exists($key, $this->data);
    }

    public function remove(string $key): void
    {
        unset($this->data[$key]);
    }
}