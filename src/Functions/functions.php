<?php


declare(strict_types=1);

if (!function_exists('gettype_custom')) {
    function gettype_custom(mixed $value): string
    {
        $type = gettype($value);

        return match ($type) {
            'integer' => 'int',
            'boolean' => 'bool',
            'double' => 'float',
            default => $type,
        };
    }
}