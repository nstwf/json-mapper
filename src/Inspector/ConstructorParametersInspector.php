<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Inspector;


use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Object\ObjectDescriptorBuilder;
use Nstwf\JsonMapper\Property\PropertyDescriptor;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Type\TypeMap;
use Psr\SimpleCache\CacheInterface;
use ReflectionNamedType;
use ReflectionParameter;
use ReflectionUnionType;


final class ConstructorParametersInspector implements Inspector
{
    public function __construct(
        private CacheInterface $cache
    ) {
    }

    public function handle(ReflectionWrapper $wrapper): ObjectDescriptor
    {
        $cacheKey = sprintf("%s-%s", __CLASS__, $wrapper->getClassName());

        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $builder = new ObjectDescriptorBuilder();

        if ($constructor = $wrapper->getReflectionClass()->getConstructor()) {
            foreach ($constructor->getParameters() as $parameter) {
                $property = $this->buildProperty($parameter);
                $builder->addProperty($property);
            }
        }

        $objectDescriptor = $builder->build();

        $this->cache->set($cacheKey, $objectDescriptor);

        return $objectDescriptor;
    }

    private function buildProperty(ReflectionParameter $parameter): PropertyDescriptor
    {
        $parameterType = $parameter->getType();

        if ($parameterType instanceof ReflectionNamedType) {
            return $this->namedType($parameter);
        }

        if ($parameterType instanceof ReflectionUnionType) {
            return $this->unionType($parameter);
        }

        return $this->undefinedType($parameter);
    }

    private function namedType(ReflectionParameter $parameter): PropertyDescriptor
    {
        $parameterName = $parameter->getName();
        $parameterType = $parameter->getType();
        $typeName = $parameterType->getName();

        $types = [];

        if ($typeName == 'array') {
            $types[] = new TypeDescriptor('mixed', true);
        } elseif ($typeName == 'mixed') {
            $types[] = new TypeDescriptor('mixed', false);
            $types[] = new TypeDescriptor('mixed', true);
        } else {
            $types[] = new TypeDescriptor($typeName, false);
        }

        return (new PropertyDescriptorBuilder())
            ->setName($parameterName)
            ->setIsNullable($parameter->allowsNull())
            ->setTypes(new TypeMap(...$types))
            ->build();
    }

    private function unionType(ReflectionParameter $parameter): PropertyDescriptor
    {
        $types = [];

        $typeNames = array_map(
            fn(ReflectionNamedType $namedType) => $namedType->getName(),
            $parameter->getType()->getTypes()
        );

        foreach ($typeNames as $typeName) {
            if ($typeName === 'array') {
                $types[] = new TypeDescriptor('mixed', true);
            } else {
                $types[] = new TypeDescriptor($typeName, false);
            }
        }

        return (new PropertyDescriptorBuilder())
            ->setName($parameter->getName())
            ->setIsNullable($parameter->allowsNull())
            ->setTypes(new TypeMap(...$types))
            ->build();
    }

    private function undefinedType(ReflectionParameter $parameter): PropertyDescriptor
    {
        return (new PropertyDescriptorBuilder())
            ->setName($parameter->getName())
            ->setIsNullable(true)
            ->setTypes(new TypeMap(
                new TypeDescriptor('mixed', false),
                new TypeDescriptor('mixed', true),
            ))
            ->build();
    }
}