<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Inspector;


use Nstwf\JsonMapper\Attribute\CustomName;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Object\ObjectDescriptorBuilder;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Psr\SimpleCache\CacheInterface;


final class CustomNameAttributeInspector implements Inspector
{
    public function __construct(
        private CacheInterface $cache
    ) {
    }

    public function handle(ReflectionWrapper $wrapper): ObjectDescriptor
    {
        $cacheKey = sprintf("%s-%s", __CLASS__, $wrapper->getClassName());

        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $builder = new ObjectDescriptorBuilder();

        $properties = $wrapper->getReflectionClass()->getProperties();

        foreach ($properties as $property) {
            $attributes = $property->getAttributes(CustomName::class);

            if (!empty($attributes)) {
                /** @var CustomName $attribute */
                $reflectionAttribute = current($attributes);
                $attribute = $reflectionAttribute->newInstance();
                $customName = $attribute->getCustomName();

                $builder->addProperty(
                    (new PropertyDescriptorBuilder())
                        ->setName($property->getName())
                        ->setCustomName($customName)
                        ->build()
                );
            }
        }

        $objectDescriptor = $builder->build();

        $this->cache->set($cacheKey, $objectDescriptor);

        return $objectDescriptor;
    }
}