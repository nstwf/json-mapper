<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Inspector;


use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;


interface Inspector
{
    public function handle(ReflectionWrapper $wrapper): ObjectDescriptor;
}