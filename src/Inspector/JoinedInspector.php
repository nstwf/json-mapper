<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Inspector;


use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Psr\SimpleCache\CacheInterface;


final class JoinedInspector implements Inspector
{
    /** @var Inspector[] */
    private array $inspectors;

    public function __construct(
        private CacheInterface $cache,
        Inspector...$inspectors
    ) {
        $this->inspectors = $inspectors;
    }

    public function handle(ReflectionWrapper $wrapper): ObjectDescriptor
    {
        $cacheKey = sprintf("%s-%s", __CLASS__, $wrapper->getClassName());

        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }

        $objectDescriptor = new ObjectDescriptor();

        foreach ($this->inspectors as $inspector) {
            $objectDescriptor = $objectDescriptor->merge($inspector->handle($wrapper));
        }

        $this->cache->set($cacheKey, $objectDescriptor);

        return $objectDescriptor;
    }
}