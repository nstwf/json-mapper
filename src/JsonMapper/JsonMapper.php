<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\JsonMapper;


use Nstwf\JsonMapper\Data\DataMap;
use Nstwf\JsonMapper\Inspector\Inspector;
use Nstwf\JsonMapper\Object\Mapper\ObjectMapper;
use Nstwf\JsonMapper\OperationResult\OperationResult;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;


class JsonMapper
{
    public function __construct(
        private Inspector $inspector,
        private ObjectMapper $objectBuilder
    ) {
    }

    public function mapObject(\stdClass $data, string $className): OperationResult
    {
        $reflectionWrapper = new ReflectionWrapper($className);
        $objectDescriptor = $this->inspector->handle($reflectionWrapper);

        $dataMap = DataMap::create($data);

        return $this->objectBuilder->map(
            $this,
            $reflectionWrapper,
            $objectDescriptor,
            $dataMap
        );
    }

    public function mapArray(array $data, string $className): OperationResult
    {
        $results = [];

        foreach ($data as $value) {
            $result = $this->mapObject($value, $className);

            if ($result->isError()) {
                return $result;
            }

            $results[] = $result->value();
        }

        return OperationResult::success($results);
    }
}