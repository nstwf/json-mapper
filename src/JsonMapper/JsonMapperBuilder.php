<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\JsonMapper;


use Nstwf\JsonMapper\Inspector\Inspector;
use Nstwf\JsonMapper\Object\Mapper\ObjectMapper;


final class JsonMapperBuilder
{
    private Inspector $inspector;
    private ObjectMapper $objectMapper;

    public function build(): JsonMapper
    {
        return new JsonMapper(
            $this->inspector,
            $this->objectMapper
        );
    }

    public function withInspector(Inspector $inspector): self
    {
        $this->inspector = $inspector;

        return $this;
    }

    public function withObjectMapper(ObjectMapper $objectMapper): self
    {
        $this->objectMapper = $objectMapper;

        return $this;
    }
}