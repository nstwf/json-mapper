<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Object\Mapper;


use Nstwf\JsonMapper\Property\Mapper\PropertyMapper;


final class ObjectMapperBuilder
{
    private PropertyMapper $mapper;

    public function build(): ObjectMapper
    {
        return new ObjectMapper(
            $this->mapper,
        );
    }

    public function withPropertyMapper(PropertyMapper $mapper): self
    {
        $this->mapper = $mapper;

        return $this;
    }
}