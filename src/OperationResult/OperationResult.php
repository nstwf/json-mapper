<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\OperationResult;


class OperationResult
{
    private function __construct(
        private readonly bool $isSuccess,
        private readonly mixed $value,
        private readonly ?string $errorMessage
    ) {
    }

    public static function success(mixed $value): OperationResult
    {
        return new self(true, $value, null);
    }

    public static function error(?string $error = null): OperationResult
    {
        return new self(false, null, $error);
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function isError(): bool
    {
        return !$this->isSuccess();
    }

    public function errorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function value(): mixed
    {
        return $this->value;
    }
}