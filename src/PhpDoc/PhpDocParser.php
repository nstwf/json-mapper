<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\PhpDoc;


final class PhpDocParser
{

    public static function parseVarAnnotation(string $docComment): string
    {
        $regex = '/(?<=@var)[\s]+(?P<data>[\w\|\[\]\-\_\\\]+)/';

        $docComment = substr($docComment, 3, -2);

        if (preg_match($regex, $docComment, $match)) {
            return trim($match['data']);
        }

        return '';
    }

    public static function parseParamAnnotation(string $docComment, string $property): string
    {
        $regex = '/(?<=@param)[\s]+(?P<data>[\w\|\[\]\-\_\\\]+)\s+\$' . $property . '/';

        $docComment = substr($docComment, 3, -2);

        if (preg_match($regex, $docComment, $match)) {
            return trim($match['data']);
        }

        return '';
    }
}