<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Reflection;


use Nstwf\ExtendedReflectionClass\ExtendedReflectionClass;


class ReflectionWrapper
{
    private ExtendedReflectionClass $reflectionClass;

    public function __construct(
        private string $className
    ) {
        $this->reflectionClass = new ExtendedReflectionClass($this->className);
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function getReflectionClass(): ExtendedReflectionClass
    {
        return $this->reflectionClass;
    }
}