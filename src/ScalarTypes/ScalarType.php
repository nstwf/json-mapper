<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\ScalarTypes;


enum ScalarType: string
{
    case INT = 'int';
    case BOOL = 'bool';
    case FLOAT = 'float';
    case STRING = 'string';

    case MIXED = 'mixed';

    public static function isScalar(string $type): bool
    {
        return self::tryFrom($type) !== null;
    }
}