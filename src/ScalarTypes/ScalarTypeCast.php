<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\ScalarTypes;


final class ScalarTypeCast
{
    public static function cast(ScalarType $scalarType, mixed $value): mixed
    {
        return match ($scalarType) {
            ScalarType::INT    => (int)$value,
            ScalarType::BOOL   => filter_var($value, FILTER_VALIDATE_BOOL),
            ScalarType::FLOAT  => (float)$value,
            ScalarType::STRING => (string)$value,
            default            => $value,
        };
    }
}