<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Type;


final class TypeMap implements \JsonSerializable
{
    /** @var TypeDescriptor[] */
    private array $types;

    public function __construct(TypeDescriptor ...$type)
    {
        $this->types = $type;
    }

    public function merge(self $self): self
    {
        foreach ($self->types as $type) {
            if (!$this->has($type->getName(), $type->isArray())) {
                if ($type->isMixed()) {
                    if (!$this->hasIsArrayType($type->isArray())) {
                        $this->add($type);
                    }
                } else {
                    $this->remove('mixed', $type->isArray());
                    $this->add($type);
                }
            }
        }

        return $this;
    }

    public function first(): TypeDescriptor
    {
        return reset($this->types);
    }

    public function isEquals(self $self): bool
    {
        return count(array_diff($this->types, $self->types)) === 0;
    }

    public function add(TypeDescriptor $type): self
    {
        $this->types[] = $type;

        return $this;
    }

    /**
     * @return TypeDescriptor[]
     */
    public function all(): array
    {
        return $this->types;
    }

    public function has(string $typeName, bool $isArray): bool
    {
        $types = array_filter(
            $this->types,
            fn(TypeDescriptor $type) => $type->getName() === $typeName && $type->isArray() === $isArray,
        );

        return count($types) > 0;
    }

    private function hasIsArrayType(bool $isArray): bool
    {
        $types = array_filter(
            $this->types,
            fn(TypeDescriptor $type) => $type->isArray() === $isArray,
        );

        return count($types) > 0;
    }

    public function remove(string $typeName, bool $isArray): void
    {
        $this->types = array_filter(
            $this->types,
            fn(TypeDescriptor $type) => $type->getName() !== $typeName || $type->isArray() !== $isArray,
        );
    }

    public function isUnion(): bool
    {
        return count($this->types) > 1;
    }

    public function isEmpty(): bool
    {
        return count($this->types) === 0;
    }

    public function __toString(): string
    {
        return implode(', ', $this->types);
    }

    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }
}