<?php

/** @noinspection PhpExpressionResultUnusedInspection */


namespace Nstwf\JsonMapper\Unit\Cache;


use Nstwf\JsonMapper\Cache\NullCache;
use PHPUnit\Framework\TestCase;


class NullCacheTest extends TestCase
{
    public function testSetDoesNotAddToCache()
    {
        $cache = new NullCache();

        /** @noinspection PhpExpressionResultUnusedInspection */
        $cache->set('1', 'value1');

        $this->assertFalse($cache->has('1'));
        $this->assertNull($cache->get('1'));
    }

    public function testGetAlwaysReturnDefaultValue()
    {
        $cache = new NullCache();

        $cache->set('key1', 'value1');

        $this->assertFalse($cache->has('key1'));
        $this->assertEquals('default', $cache->get('key1', 'default'));
        $this->assertFalse($cache->has('key2'));
        $this->assertEquals('default5', $cache->get('key2', 'default5'));
    }

    public function testDeleteValueDoesNotDoAnythingAndCacheStillEmpty()
    {
        $cache = new NullCache();

        $cache->set('key1', 'value1');
        $cache->delete('key1');

        $this->assertFalse($cache->has('key1'));
    }

    public function testClearDoesNotDoAnything()
    {
        $cache = new NullCache();

        $cache->set('key1', 'value1');
        $cache->set('key2', 'value2');
        $cache->set('key3', 'value3');

        $cache->clear();

        $this->assertFalse($cache->has('key1'));
        $this->assertFalse($cache->has('key2'));
        $this->assertFalse($cache->has('key3'));
    }

    public function testSetMultipleDoesNotAddToCache()
    {
        $cache = new NullCache();

        $values = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
        ];

        $cache->setMultiple($values);

        $multiple = $cache->getMultiple(['key1', 'key2', 'key3']);

        $this->assertEquals([
            'key1' => null,
            'key2' => null,
            'key3' => null,
        ], $multiple);
    }

    public function testDeleteMultipleDoesNotDoAnythingAndCacheStillEmpty()
    {
        $cache = new NullCache();

        $cache->setMultiple([
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
        ]);

        $cache->deleteMultiple(['key1', 'key3']);

        $multiple = $cache->getMultiple(['key1', 'key2', 'key3']);

        $this->assertEquals([
            'key1' => null,
            'key2' => null,
            'key3' => null,
        ], $multiple);
    }
}