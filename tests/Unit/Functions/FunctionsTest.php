<?php


namespace Nstwf\JsonMapper\Unit\Functions;


use PHPUnit\Framework\TestCase;


class FunctionsTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testGettypeCustom(mixed $value, string $type)
    {
        $this->assertEquals($type, gettype_custom($value));
    }

    public function dataProvider()
    {
        return [
            'int'    => [1, 'int'],
            'bool'   => [false, 'bool'],
            'string' => ['value', 'string'],
            'float'  => [5.55, 'float'],
        ];
    }
}