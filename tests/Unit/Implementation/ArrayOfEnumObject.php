<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


use Nstwf\JsonMapper\Unit\Implementation\Enum\IntEnum;
use Nstwf\JsonMapper\Unit\Implementation\Enum\StringEnum;


final class ArrayOfEnumObject
{
    /** @var IntEnum[] */
    private array $intEnums;
    /** @var StringEnum[] */
    private array $stringEnums;

    /**
     * @param IntEnum[]    $intEnums
     * @param StringEnum[] $stringEnums
     */
    public function __construct(
        array $intEnums,
        array $stringEnums
    ) {
        $this->intEnums = $intEnums;
        $this->stringEnums = $stringEnums;
    }

    /**
     * @param IntEnum[] $intEnums
     */
    public function setIntEnums(array $intEnums): void
    {
        $this->intEnums = $intEnums;
    }

    /**
     * @param StringEnum[] $stringEnums
     */
    public function setStringEnums(array $stringEnums): void
    {
        $this->stringEnums = $stringEnums;
    }

    public function getIntEnums(): array
    {
        return $this->intEnums;
    }

    public function getStringEnums(): array
    {
        return $this->stringEnums;
    }
}