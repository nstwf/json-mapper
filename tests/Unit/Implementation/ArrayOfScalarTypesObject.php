<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


final class ArrayOfScalarTypesObject
{
    /** @var int[] */
    private array $intArray;
    /** @var string[] */
    private array $stringArray;
    /** @var float[] */
    private array $floatArray;
    /** @var bool[] */
    private array $boolArray;

    /**
     * @param int[]    $intArray
     * @param string[] $stringArray
     * @param float[]  $floatArray
     * @param bool[]   $boolArray
     */
    public function __construct(array $intArray, array $stringArray, array $floatArray, array $boolArray)
    {
        $this->intArray = $intArray;
        $this->stringArray = $stringArray;
        $this->floatArray = $floatArray;
        $this->boolArray = $boolArray;
    }

    /**
     * @param array $intArray
     */
    public function setIntArray(array $intArray): void
    {
        $this->intArray = $intArray;
    }

    public function setStringArray(array $stringArray): void
    {
        $this->stringArray = $stringArray;
    }

    public function setFloatArray(array $floatArray): void
    {
        $this->floatArray = $floatArray;
    }

    public function setBoolArray(array $boolArray): void
    {
        $this->boolArray = $boolArray;
    }

    public function getIntArray(): array
    {
        return $this->intArray;
    }

    public function getStringArray(): array
    {
        return $this->stringArray;
    }

    public function getFloatArray(): array
    {
        return $this->floatArray;
    }

    public function getBoolArray(): array
    {
        return $this->boolArray;
    }
}