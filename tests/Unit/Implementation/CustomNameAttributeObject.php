<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


use Nstwf\JsonMapper\Attribute\CustomName;


final class CustomNameAttributeObject
{
    #[CustomName('custom_name')]
    private $property;
}