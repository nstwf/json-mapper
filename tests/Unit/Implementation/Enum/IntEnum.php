<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation\Enum;


enum IntEnum: int
{
    case ONE = 1;
    case TWO = 2;
}