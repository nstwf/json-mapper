<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


use Nstwf\JsonMapper\Unit\Implementation\Enum\IntEnum;


final class EnumObject
{
    /** @var IntEnum */
    private IntEnum $enum;
    /** @var IntEnum|null */
    private ?IntEnum $nullableEnum;

    /**
     * @param IntEnum      $enum
     * @param IntEnum|null $nullableEnum
     */
    public function __construct(
        IntEnum $enum,
        ?IntEnum $nullableEnum
    ) {
        $this->enum = $enum;
        $this->nullableEnum = $nullableEnum;
    }

    public function setEnum(IntEnum $enum): void
    {
        $this->enum = $enum;
    }

    public function setNullableEnum(?IntEnum $nullableEnum): void
    {
        $this->nullableEnum = $nullableEnum;
    }

    public function getEnum(): IntEnum
    {
        return $this->enum;
    }

    public function getNullableEnum(): ?IntEnum
    {
        return $this->nullableEnum;
    }
}