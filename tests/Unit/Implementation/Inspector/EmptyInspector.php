<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation\Inspector;


use Nstwf\JsonMapper\Inspector\Inspector;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;


final class EmptyInspector implements Inspector
{
    public function handle(ReflectionWrapper $wrapper): ObjectDescriptor
    {
        return new ObjectDescriptor();
    }
}