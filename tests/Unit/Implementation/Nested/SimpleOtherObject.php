<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation\Nested;


final class SimpleOtherObject
{
    /** @var */
    private $property;

    /**
     * @param $property
     */
    public function __construct($property)
    {
        $this->property = $property;
    }

    public function getProperty()
    {
        return $this->property;
    }

    /**
     * @param $property
     *
     * @return void
     */
    public function setNoType($property): void
    {
        $this->property = $property;
    }
}