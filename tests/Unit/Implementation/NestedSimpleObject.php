<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


use Nstwf\JsonMapper\Unit\Implementation\Nested\SimpleObject;


final class NestedSimpleObject
{
    /** @var SimpleObject */
    private SimpleObject $simpleObject;
    /** @var SimpleObject|null */
    private ?SimpleObject $simpleNullableObject;

    /**
     * @param SimpleObject      $simpleObject
     * @param SimpleObject|null $simpleNullableObject
     */
    public function __construct(
        SimpleObject $simpleObject,
        ?SimpleObject $simpleNullableObject,
    ) {
        $this->simpleObject = $simpleObject;
        $this->simpleNullableObject = $simpleNullableObject;
    }

    /**
     * @param SimpleObject $simpleObject
     */
    public function setSimpleObject(SimpleObject $simpleObject): void
    {
        $this->simpleObject = $simpleObject;
    }

    /**
     * @param SimpleObject|null $simpleNullableObject
     */
    public function setSimpleNullableObject(?SimpleObject $simpleNullableObject): void
    {
        $this->simpleNullableObject = $simpleNullableObject;
    }

    /**
     * @return SimpleObject
     */
    public function getSimpleObject(): SimpleObject
    {
        return $this->simpleObject;
    }

    /**
     * @return SimpleObject|null
     */
    public function getSimpleNullableObject(): ?SimpleObject
    {
        return $this->simpleNullableObject;
    }
}