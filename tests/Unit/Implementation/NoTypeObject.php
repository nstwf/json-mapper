<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


final class NoTypeObject
{
    /** @var */
    private $noType;

    private $noTypeNoDoc;

    /**
     * @param $noType
     */
    public function __construct(
        $noType,
        $noTypeNoDoc
    ) {

        $this->noType = $noType;
        $this->noTypeNoDoc = $noTypeNoDoc;
    }

    public function getNoType()
    {
        return $this->noType;
    }

    /**
     * @param $noType
     */
    public function setNoType($noType): void
    {
        $this->noType = $noType;
    }

    public function getNoTypeNoDoc()
    {
        return $this->noTypeNoDoc;
    }

    public function setNoTypeNoDoc($noTypeNoDoc): void
    {
        $this->noTypeNoDoc = $noTypeNoDoc;
    }
}