<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


final class PrivateConstructorObject
{
    private function __construct()
    {

    }
}