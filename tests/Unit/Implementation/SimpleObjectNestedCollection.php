<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


use Nstwf\JsonMapper\Unit\Implementation\Collection\SimpleObjectCollection;


final class SimpleObjectNestedCollection
{
    public function __construct(
        public SimpleObjectCollection $simpleObjectCollection
    ) {
    }
}