<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


use Nstwf\JsonMapper\Unit\Implementation\Enum\IntEnum;
use Nstwf\JsonMapper\Unit\Implementation\Enum\StringEnum;


final class UnionEnumObject
{
    /** @var IntEnum|StringEnum */
    private IntEnum|StringEnum $enum;
    /** @var IntEnum|StringEnum|null */
    private IntEnum|null|StringEnum $nullableEnum;

    /**
     * @param IntEnum|StringEnum      $enum
     * @param IntEnum|StringEnum|null $nullableEnum
     */
    public function __construct(
        IntEnum|StringEnum $enum,
        IntEnum|StringEnum|null $nullableEnum
    ) {
        $this->enum = $enum;
        $this->nullableEnum = $nullableEnum;
    }

    /**
     * @param StringEnum|IntEnum $enum
     */
    public function setEnum(StringEnum|IntEnum $enum): void
    {
        $this->enum = $enum;
    }

    /**
     * @param StringEnum|IntEnum|null $nullableEnum
     */
    public function setNullableEnum(StringEnum|IntEnum|null $nullableEnum): void
    {
        $this->nullableEnum = $nullableEnum;
    }

    public function getEnum(): IntEnum|StringEnum
    {
        return $this->enum;
    }

    public function getNullableEnum(): IntEnum|StringEnum|null
    {
        return $this->nullableEnum;
    }
}