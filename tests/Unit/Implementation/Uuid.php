<?php


declare(strict_types=1);


namespace Nstwf\JsonMapper\Unit\Implementation;


final class Uuid
{
    public function __construct(
        public string $value
    ) {
    }
}