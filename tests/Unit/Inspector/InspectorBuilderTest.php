<?php


namespace Nstwf\JsonMapper\Unit\Inspector;


use Nstwf\JsonMapper\Inspector\Inspector;
use Nstwf\JsonMapper\Inspector\InspectorBuilder;
use PHPUnit\Framework\TestCase;


class InspectorBuilderTest extends TestCase
{
    public function testCreateWithoutInspectors()
    {
        $builder = new InspectorBuilder();

        $this->expectException(\RuntimeException::class);
        $builder->build();
    }

    public function testCreateWithAllInspectors()
    {
        $result = (new InspectorBuilder())
            ->withTypesProperties()
            ->withPropertyAnnotations()
            ->withConstructorParameters()
            ->withConstructorAnnotations()
            ->withCustomNameAttribute()
            ->build();

        $this->assertInstanceOf(Inspector::class, $result);
    }
}