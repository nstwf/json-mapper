<?php


namespace Nstwf\JsonMapper\Unit\JsonMapper;


use Nstwf\JsonMapper\Inspector\Inspector;
use Nstwf\JsonMapper\JsonMapper\JsonMapper;
use Nstwf\JsonMapper\Object\Mapper\ObjectMapper;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\OperationResult\OperationResult;
use PHPUnit\Framework\TestCase;


class JsonMapperTest extends TestCase
{
    public function testMapObject()
    {
        $inspector = $this->createMock(Inspector::class);
        $inspector->method('handle')
            ->willReturn(new ObjectDescriptor());

        $objectMapper = $this->createMock(ObjectMapper::class);
        $objectMapper->expects($this->once())
            ->method('map')
            ->willReturn(OperationResult::success(new \stdClass()));

        $jsonMapper = new JsonMapper($inspector, $objectMapper);

        $result = $jsonMapper->mapObject(new \stdClass(), __CLASS__);
        $this->assertTrue($result->isSuccess());
        $this->assertIsObject($result->value());
    }

    public function testMapArray()
    {
        $inspector = $this->createMock(Inspector::class);
        $inspector->method('handle')
            ->willReturn(new ObjectDescriptor());

        $objectMapper = $this->createMock(ObjectMapper::class);
        $objectMapper->expects($this->once())
            ->method('map')
            ->willReturn(OperationResult::success([new \stdClass()]));

        $jsonMapper = new JsonMapper($inspector, $objectMapper);

        $operationResult = $jsonMapper->mapArray([new \stdClass()], __CLASS__);
        $this->assertTrue($operationResult->isSuccess());
        $this->assertIsArray($operationResult->value());
    }

    public function testMapArrayReturnError()
    {
        $inspector = $this->createMock(Inspector::class);
        $inspector->method('handle')
            ->willReturn(new ObjectDescriptor());

        $objectMapper = $this->createMock(ObjectMapper::class);
        $objectMapper->expects($this->once())
            ->method('map')
            ->willReturn(OperationResult::error('CUSTOM_ERROR'));

        $jsonMapper = new JsonMapper($inspector, $objectMapper);

        $operationResult = $jsonMapper->mapArray([new \stdClass()], __CLASS__);
        $this->assertTrue($operationResult->isError());
        $this->assertEquals('CUSTOM_ERROR', $operationResult->errorMessage());
    }
}