<?php


namespace Nstwf\JsonMapper\Unit\Object\Mapper;


use Nstwf\JsonMapper\Data\DataMap;
use Nstwf\JsonMapper\JsonMapper\JsonMapper;
use Nstwf\JsonMapper\Object\Mapper\ObjectMapper;
use Nstwf\JsonMapper\Object\ObjectDescriptorBuilder;
use Nstwf\JsonMapper\OperationResult\OperationResult;
use Nstwf\JsonMapper\Property\Mapper\PropertyMapper;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Reflection\ReflectionWrapper;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Type\TypeMap;
use Nstwf\JsonMapper\Unit\Implementation\NoConstructorObject;
use Nstwf\JsonMapper\Unit\Implementation\PrivateConstructorObject;
use Nstwf\JsonMapper\Unit\Implementation\Uuid;
use PHPUnit\Framework\TestCase;


class ObjectMapperTest extends TestCase
{
    private PropertyMapper $propertyMapper;
    private JsonMapper $jsonMapper;

    protected function setUp(): void
    {
        $this->propertyMapper = $this->createMock(PropertyMapper::class);
        $this->jsonMapper = $this->createMock(JsonMapper::class);

        parent::setUp();
    }

    public function testMapObjectUsingOnlyConstructor()
    {
        $this->propertyMapper->method('mapValue')
            ->with($this->jsonMapper, new TypeMap(new TypeDescriptor('int', false)), 5)
            ->willReturn(OperationResult::success(5));

        $class = new class (5) {
            public function __construct(
                public $int
            ) {
            }
        };
        $reflection = new ReflectionWrapper(get_class($class));
        $mapper = new ObjectMapper($this->propertyMapper);

        $dataMap = DataMap::create([
            'int' => 5,
        ]);

        $objectDescriptor = (new ObjectDescriptorBuilder())
            ->addProperty(
                (new PropertyDescriptorBuilder())
                    ->setName('int')
                    ->addType(new TypeDescriptor('int', false))
                    ->setIsNullable(false)
                    ->build()
            )
            ->build();

        $result = $mapper->map($this->jsonMapper, $reflection, $objectDescriptor, $dataMap);
        $this->assertEquals($class, $result->value());
    }

    public function testMapObjectUsingConstructorOnlyWithDefaultValue()
    {
        $this->propertyMapper->method('mapValue')
            ->with($this->jsonMapper, new TypeMap(new TypeDescriptor('int', false)), 5)
            ->willReturn(OperationResult::success(5));

        $class = new class (5) {
            public function __construct(
                public $int,
                public $default = 5
            ) {
            }
        };
        $reflection = new ReflectionWrapper(get_class($class));
        $mapper = new ObjectMapper($this->propertyMapper);

        $dataMap = DataMap::create([
            'int' => 5,
        ]);

        $objectDescriptor = (new ObjectDescriptorBuilder())
            ->addProperty(
                (new PropertyDescriptorBuilder())
                    ->setName('int')
                    ->addType(new TypeDescriptor('int', false))
                    ->setIsNullable(false)
                    ->build()
            )
            ->build();

        $result = $mapper->map($this->jsonMapper, $reflection, $objectDescriptor, $dataMap);
        $this->assertEquals($class, $result->value());
    }

    public function testMapObjectUsingConstructorToNullablePropertyWithNull()
    {
        $class = new class (null) {
            public function __construct(
                public $default
            ) {
            }
        };
        $reflection = new ReflectionWrapper(get_class($class));
        $mapper = new ObjectMapper($this->propertyMapper);

        $dataMap = DataMap::create([
            'default' => null,
        ]);

        $objectDescriptor = (new ObjectDescriptorBuilder())
            ->addProperty(
                (new PropertyDescriptorBuilder())
                    ->setName('default')
                    ->addType(new TypeDescriptor('int', false))
                    ->setIsNullable(true)
                    ->build()
            )
            ->build();

        $result = $mapper->map($this->jsonMapper, $reflection, $objectDescriptor, $dataMap);
        $this->assertEquals($class, $result->value());
    }

    public function testMapObjectUsingConstructorToNotNullablePropertyWithNull()
    {
        $class = new class (null) {
            public function __construct(
                public $default
            ) {
            }
        };
        $reflection = new ReflectionWrapper(get_class($class));
        $mapper = new ObjectMapper($this->propertyMapper);

        $dataMap = DataMap::create([
            'default' => null,
        ]);

        $objectDescriptor = (new ObjectDescriptorBuilder())
            ->addProperty(
                (new PropertyDescriptorBuilder())
                    ->setName('default')
                    ->addType(new TypeDescriptor('int', false))
                    ->setIsNullable(false)
                    ->build()
            )
            ->build();

        $result = $mapper->map($this->jsonMapper, $reflection, $objectDescriptor, $dataMap);

        $this->assertTrue($result->isError());
    }

    public function testMapObjectWithErrorMappingToSpecifyErrorMessage()
    {
        $this->propertyMapper->method('mapValue')
            ->willReturn(OperationResult::error('CUSTOM_ERROR'));

        $reflection = new ReflectionWrapper(Uuid::class);
        $mapper = new ObjectMapper($this->propertyMapper);

        $dataMap = DataMap::create([
            'value' => 6.66,
        ]);

        $objectDescriptor = (new ObjectDescriptorBuilder())
            ->addProperty(
                (new PropertyDescriptorBuilder())
                    ->setName('value')
                    ->addType(new TypeDescriptor('string', false))
                    ->setIsNullable(false)
                    ->build()
            )
            ->build();

        $result = $mapper->map($this->jsonMapper, $reflection, $objectDescriptor, $dataMap);

        $this->assertEquals('Constructor parameter (value) for class (Nstwf\JsonMapper\Unit\Implementation\Uuid): CUSTOM_ERROR', $result->errorMessage());
        $this->assertTrue($result->isError());
    }

    public function testMapObjectWithoutConstructor()
    {
        $reflection = new ReflectionWrapper(NoConstructorObject::class);
        $mapper = new ObjectMapper($this->propertyMapper);

        $dataMap = DataMap::create([]);

        $objectDescriptor = (new ObjectDescriptorBuilder())->build();

        $result = $mapper->map($this->jsonMapper, $reflection, $objectDescriptor, $dataMap);

        $this->assertEquals(new NoConstructorObject(), $result->value());
    }

    public function testMapObjectWithPrivateConstructor()
    {
        $reflection = new ReflectionWrapper(PrivateConstructorObject::class);
        $mapper = new ObjectMapper($this->propertyMapper);

        $dataMap = DataMap::create([]);

        $objectDescriptor = (new ObjectDescriptorBuilder())->build();

        $mapObjectResult = $mapper->map($this->jsonMapper, $reflection, $objectDescriptor, $dataMap);
        $this->assertFalse($mapObjectResult->isSuccess());
    }

    public function testMapObjectUsingOnlyConstructorWithoutData()
    {
        $this->propertyMapper->method('mapValue')
            ->with($this->jsonMapper, new TypeMap(new TypeDescriptor('int', false)), 5)
            ->willReturn(OperationResult::success(5));

        $class = new class (5, 5) {
            public function __construct(
                public $int,
                public $string
            ) {
            }
        };
        $reflection = new ReflectionWrapper(get_class($class));
        $mapper = new ObjectMapper($this->propertyMapper);

        $dataMap = DataMap::create([
            'int' => 5,
        ]);

        $objectDescriptor = (new ObjectDescriptorBuilder())
            ->addProperty(
                (new PropertyDescriptorBuilder())
                    ->setName('int')
                    ->addType(new TypeDescriptor('int', false))
                    ->setIsNullable(false)
                    ->build()
            )
            ->build();

        $mapObjectResult = $mapper->map($this->jsonMapper, $reflection, $objectDescriptor, $dataMap);
        $this->assertFalse($mapObjectResult->isSuccess());
    }
}