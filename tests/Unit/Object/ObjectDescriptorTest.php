<?php


namespace Nstwf\JsonMapper\Unit\Object;


use Nstwf\JsonMapper\Asserts\ObjectDescriptorAsserts;
use Nstwf\JsonMapper\Object\ObjectDescriptor;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Property\PropertyMap;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use PHPUnit\Framework\TestCase;


class ObjectDescriptorTest extends TestCase
{
    public function testMerge()
    {
        $objectDescriptor = new ObjectDescriptor(
            (new PropertyMap())
                ->add((new PropertyDescriptorBuilder())
                    ->setName('nameInt')
                    ->addType(new TypeDescriptor('int', false))
                    ->setIsNullable(false)
                    ->build()
                )
        );

        $anotherObjectDescriptor = new ObjectDescriptor(
            (new PropertyMap())
                ->add((new PropertyDescriptorBuilder())
                    ->setName('nameString')
                    ->addType(new TypeDescriptor('string', false))
                    ->setIsNullable(false)
                    ->build()
                )
        );

        $objectDescriptor = $objectDescriptor->merge($anotherObjectDescriptor);

        $objectDescriptorAsserts = (new ObjectDescriptorAsserts($objectDescriptor));
        $propertyMapAsserts = $objectDescriptorAsserts->propertyMapAsserts();

        $propertyMapAsserts->assertCount(2);
    }

    public function testConvertToJson()
    {
        $objectDescriptor = new ObjectDescriptor(
            (new PropertyMap())
                ->add((new PropertyDescriptorBuilder())
                    ->setName('nameInt')
                    ->addType(new TypeDescriptor('int', false))
                    ->setIsNullable(false)
                    ->build()
                )
                ->add((new PropertyDescriptorBuilder())
                    ->setName('nameString')
                    ->addType(new TypeDescriptor('string', false))
                    ->setIsNullable(false)
                    ->build()
                )
        );

        $this->assertEquals(
            '{"propertyMap":{"properties":{"nameInt":{"name":"nameInt","types":{"types":[{"name":"int","isArray":false}]},"isNullable":false,"customName":null},"nameString":{"name":"nameString","types":{"types":[{"name":"string","isArray":false}]},"isNullable":false,"customName":null}}}}',
            json_encode($objectDescriptor)
        );
    }
}