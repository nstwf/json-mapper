<?php


namespace Nstwf\JsonMapper\Unit\PhpDoc;


use Nstwf\JsonMapper\PhpDoc\PhpDocParser;
use PHPUnit\Framework\TestCase;


class PhpDocParserTest extends TestCase
{
    public function testVarEmptyComment()
    {
        $doc = '/** */';

        $this->assertEquals('', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testVarSingleType()
    {
        $doc = '/** @var int */';

        $this->assertEquals('int', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testVarSingleTypeWithMultiline()
    {
        $doc = '/** 
                    @var int 
                */';

        $this->assertEquals('int', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testVarSingleClass()
    {
        $doc = '/** @var Class */';

        $this->assertEquals('Class', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testVarSingleTypeWithText()
    {
        $doc = '/** @var int text */';

        $this->assertEquals('int', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testVarUnionType()
    {
        $doc = '/** @var int|string text */';

        $this->assertEquals('int|string', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testVarUnionTypeWithArray()
    {
        $doc = '/** @var int[]|string|array text */';

        $this->assertEquals('int[]|string|array', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testVarClassName()
    {
        $doc = '/** @var Class|Class2 text */';

        $this->assertEquals('Class|Class2', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testVarClassNameWithNamespace()
    {
        $doc = '/** @var src\Class|src\Class2 text */';

        $this->assertEquals('src\\Class|src\\Class2', PhpDocParser::parseVarAnnotation($doc));
    }

    public function testParamEmptyComment()
    {
        $doc = '/** */';

        $this->assertEquals('', PhpDocParser::parseParamAnnotation($doc, 'param'));
    }

    public function testParamNoPropertyName()
    {
        $doc = '/** @param int */';

        $this->assertEquals('', PhpDocParser::parseParamAnnotation($doc, 'param'));
    }

    public function testParamSingleType()
    {
        $doc = '/** @param int $parameter */';

        $this->assertEquals('int', PhpDocParser::parseParamAnnotation($doc, 'parameter'));
    }

    public function testParamSingleTypeWithText()
    {
        $doc = '/** @param int $parameter text */';

        $this->assertEquals('int', PhpDocParser::parseParamAnnotation($doc, 'parameter'));
    }

    public function testParamUnionType()
    {
        $doc = '/** @param int|string $parameter text */';

        $this->assertEquals('int|string', PhpDocParser::parseParamAnnotation($doc, 'parameter'));
    }

    public function testParamUnionTypeWithArray()
    {
        $doc = '/** @param int[]|string|array $parameter text */';

        $this->assertEquals('int[]|string|array', PhpDocParser::parseParamAnnotation($doc, 'parameter'));
    }

    public function testParamClassNameWithNamespace()
    {
        $doc = '/** @param src\Class|src\Class2 $parameter text */';

        $this->assertEquals('src\\Class|src\\Class2', PhpDocParser::parseParamAnnotation($doc, 'parameter'));
    }

    public function testParamClassNameWithMultipleParams()
    {
        $doc = '/** 
                    @param Class $parameter text 
                    @param Class2 $another text 
                */';

        $this->assertEquals('Class', PhpDocParser::parseParamAnnotation($doc, 'parameter'));
    }
}