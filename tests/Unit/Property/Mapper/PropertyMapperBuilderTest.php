<?php


namespace Nstwf\JsonMapper\Unit\Property\Mapper;


use Nstwf\JsonMapper\Property\Mapper\PropertyMapper;
use Nstwf\JsonMapper\Property\Mapper\PropertyMapperBuilder;
use Nstwf\JsonMapper\Registry\ClassFactoryRegistry;
use PHPUnit\Framework\TestCase;


class PropertyMapperBuilderTest extends TestCase
{
    public function testBuild()
    {
        $builder = new PropertyMapperBuilder();
        $builder->withClassFactoryRegistry(new ClassFactoryRegistry());

        $this->assertInstanceOf(PropertyMapper::class, $builder->build());
    }
}