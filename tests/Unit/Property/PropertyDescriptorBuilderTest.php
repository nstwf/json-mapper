<?php


namespace Nstwf\JsonMapper\Unit\Property;


use Nstwf\JsonMapper\Asserts\PropertyDescriptorAsserts;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use PHPUnit\Framework\TestCase;


class PropertyDescriptorBuilderTest extends TestCase
{
    public function testBuild()
    {
        $property = (new PropertyDescriptorBuilder())
            ->setName('name')
            ->addType(new TypeDescriptor('int', false))
            ->addType(new TypeDescriptor('float', true))
            ->setIsNullable(false)
            ->build();

        $asserts = new PropertyDescriptorAsserts($property);

        $asserts->assertCustomName(null)
            ->assertType('int', false)
            ->assertType('float', true)
            ->assertIsNullable(false);
    }
}