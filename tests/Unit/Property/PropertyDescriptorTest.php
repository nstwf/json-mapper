<?php


namespace Nstwf\JsonMapper\Unit\Property;


use Nstwf\JsonMapper\Property\PropertyDescriptor;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Type\TypeMap;
use PHPUnit\Framework\TestCase;


class PropertyDescriptorTest extends TestCase
{
    public function testIsEquals()
    {
        $property = new PropertyDescriptor(
            'name',
            new TypeMap(new TypeDescriptor('int', false), new TypeDescriptor('float', true)),
            false,
            null
        );
        $anotherProperty = new PropertyDescriptor(
            'name',
            new TypeMap(new TypeDescriptor('float', true), new TypeDescriptor('int', false)),
            false,
            null
        );

        $this->assertTrue($property->isEquals($anotherProperty));
    }

    public function testIsNotEqualsByName()
    {
        $property = new PropertyDescriptor(
            'name',
            new TypeMap(new TypeDescriptor('int', false), new TypeDescriptor('float', true)),
            false,
            null
        );
        $anotherProperty = new PropertyDescriptor(
            'name2',
            new TypeMap(new TypeDescriptor('float', true), new TypeDescriptor('int', false)),
            false,
            null
        );

        $this->assertFalse($property->isEquals($anotherProperty));
    }

    public function testIsNotEqualsByTypeMap()
    {
        $property = new PropertyDescriptor(
            'name',
            new TypeMap(new TypeDescriptor('int', true), new TypeDescriptor('float', true)),
            false,
            null
        );
        $anotherProperty = new PropertyDescriptor(
            'name',
            new TypeMap(new TypeDescriptor('float', true), new TypeDescriptor('int', false)),
            false,
            null
        );

        $this->assertFalse($property->isEquals($anotherProperty));
    }

    public function testIsNotEqualsByIsNullable()
    {
        $property = new PropertyDescriptor(
            'name',
            new TypeMap(new TypeDescriptor('int', false), new TypeDescriptor('float', true)),
            true,
            null
        );
        $anotherProperty = new PropertyDescriptor(
            'name',
            new TypeMap(new TypeDescriptor('float', true), new TypeDescriptor('int', false)),
            false,
            null
        );

        $this->assertFalse($property->isEquals($anotherProperty));
    }

    public function testConvertToJson()
    {
        $property = new PropertyDescriptor(
            'name2',
            new TypeMap(new TypeDescriptor('int', false), new TypeDescriptor('float', true)),
            true,
            null
        );

        $this->assertEquals(
            '{"name":"name2","types":{"types":[{"name":"int","isArray":false},{"name":"float","isArray":true}]},"isNullable":true,"customName":null}',
            json_encode($property)
        );
    }
}