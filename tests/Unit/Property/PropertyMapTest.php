<?php


namespace Nstwf\JsonMapper\Unit\Property;


use Nstwf\JsonMapper\Asserts\PropertyMapAsserts;
use Nstwf\JsonMapper\Property\PropertyDescriptorBuilder;
use Nstwf\JsonMapper\Property\PropertyMap;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use PHPUnit\Framework\TestCase;


class PropertyMapTest extends TestCase
{
    public function testAddPropertyToNewMap()
    {
        $propertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            );

        $propertyMapAsserts = new PropertyMapAsserts($propertyMap);

        $propertyMapAsserts->assertCount(1)
            ->assertProperty('name')
            ->assertType('int', false)
            ->assertIsNullable(false);
    }

    public function testAddTwoSameProperties()
    {
        $propertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            )
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            );

        $propertyMapAsserts = new PropertyMapAsserts($propertyMap);

        $propertyMapAsserts->assertCount(1)
            ->assertProperty('name')
            ->assertType('int', false)
            ->assertIsNullable(false);
    }

    public function testAddPropertyWithSameName()
    {
        $propertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            )
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('string', false))
                ->setIsNullable(false)
                ->build()
            );

        $propertyMapAsserts = new PropertyMapAsserts($propertyMap);

        $propertyMapAsserts->assertCount(1)
            ->assertProperty('name')
            ->assertType('string', false)
            ->assertIsNullable(false);
    }

    public function testAddPropertyWithDifferentNames()
    {
        $propertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('nameInt')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            )
            ->add((new PropertyDescriptorBuilder())
                ->setName('nameString')
                ->addType(new TypeDescriptor('string', false))
                ->setIsNullable(false)
                ->build()
            );

        $propertyMapAsserts = new PropertyMapAsserts($propertyMap);

        $propertyMapAsserts->assertCount(2);

        $propertyMapAsserts->assertProperty('nameInt')
            ->assertType('int', false)
            ->assertIsNullable(false);

        $propertyMapAsserts->assertProperty('nameString')
            ->assertType('string', false)
            ->assertIsNullable(false);
    }

    public function testMergeWithDifferentPropertyNames()
    {
        // Arrange
        $mainPropertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('nameInt')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            );

        $anotherPropertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('nameString')
                ->addType(new TypeDescriptor('string', false))
                ->setIsNullable(false)
                ->build()
            );

        // Act
        $propertyMap = $mainPropertyMap->merge($anotherPropertyMap);

        // Assert
        $propertyMapAsserts = new PropertyMapAsserts($propertyMap);

        $propertyMapAsserts->assertCount(2);

        $propertyMapAsserts->assertProperty('nameInt')
            ->assertType('int', false)
            ->assertIsNullable(false);

        $propertyMapAsserts->assertProperty('nameString')
            ->assertType('string', false)
            ->assertIsNullable(false);
    }

    public function testMergeWithSamePropertyNameAndDifferentTypes()
    {
        // Arrange
        $mainPropertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            );

        $anotherPropertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('string', false))
                ->setIsNullable(false)
                ->build()
            );

        // Act
        $propertyMap = $mainPropertyMap->merge($anotherPropertyMap);

        // Assert
        $propertyMapAsserts = new PropertyMapAsserts($propertyMap);

        $propertyMapAsserts->assertCount(1)
            ->assertProperty('name')
            ->assertType('int', false)
            ->assertType('string', false)
            ->assertIsNullable(false);
    }

    public function testMergeWithSamePropertyNameAndTypes()
    {
        // Arrange
        $mainPropertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            );

        $anotherPropertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('string', false))
                ->setIsNullable(false)
                ->build()
            );

        // Act
        $propertyMap = $mainPropertyMap->merge($anotherPropertyMap);

        // Assert
        $propertyMapAsserts = new PropertyMapAsserts($propertyMap);

        $propertyMapAsserts->assertCount(1)
            ->assertProperty('name')
            ->assertType('int', false)
            ->assertIsNullable(false);
    }

    public function testMergeMixedTypeMustSubstituteConcrete()
    {
        // Arrange
        $mainPropertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('mixed', false))
                ->setIsNullable(false)
                ->build()
            );

        $anotherPropertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('name')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            );;

        // Act
        $propertyMap = $mainPropertyMap->merge($anotherPropertyMap);

        // Assert
        $propertyMapAsserts = new PropertyMapAsserts($propertyMap);

        $propertyMapAsserts->assertCount(1)
            ->assertProperty('name')
            ->assertType('int', false)
            ->assertIsNullable(false);
    }

    public function testConvertToJson()
    {
        $propertyMap = (new PropertyMap())
            ->add((new PropertyDescriptorBuilder())
                ->setName('nameInt')
                ->addType(new TypeDescriptor('int', false))
                ->setIsNullable(false)
                ->build()
            )
            ->add((new PropertyDescriptorBuilder())
                ->setName('nameString')
                ->addType(new TypeDescriptor('string', false))
                ->setIsNullable(false)
                ->build()
            );

        $this->assertEquals(
            '{"properties":{"nameInt":{"name":"nameInt","types":{"types":[{"name":"int","isArray":false}]},"isNullable":false,"customName":null},"nameString":{"name":"nameString","types":{"types":[{"name":"string","isArray":false}]},"isNullable":false,"customName":null}}}',
            json_encode($propertyMap)
        );
    }
}