<?php


namespace Nstwf\JsonMapper\Unit\ScalarTypes;


use Nstwf\JsonMapper\ScalarTypes\ScalarType;
use Nstwf\JsonMapper\ScalarTypes\ScalarTypeCast;
use PHPUnit\Framework\TestCase;


class ScalarTypeCastTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testCastToScalarTypes(string $scalarType, mixed $valueToCast, mixed $expectedValue)
    {
        $scalarType = ScalarType::from($scalarType);

        $actualValue = ScalarTypeCast::cast($scalarType, $valueToCast);

        $this->assertEquals($expectedValue, $actualValue);
    }

    public function dataProvider(): array
    {
        return [
            'int to int'           => ['int', 5, 5],
            'string to int'        => ['int', '5', 5],
            'string to float'      => ['float', '5.55', 5.55],
            'int to bool true'     => ['bool', 1, true],
            'int to bool false'    => ['bool', 0, false],
            'string to bool true'  => ['bool', 'true', true],
            'string to bool false' => ['bool', 'false', false],
        ];
    }
}