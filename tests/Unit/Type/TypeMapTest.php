<?php


namespace Nstwf\JsonMapper\Unit\Type;


use Nstwf\JsonMapper\Asserts\TypeMapAsserts;
use Nstwf\JsonMapper\Type\TypeDescriptor;
use Nstwf\JsonMapper\Type\TypeMap;
use PHPUnit\Framework\TestCase;


class TypeMapTest extends TestCase
{
    public function testAdd()
    {
        // Arrange
        $typeMap = new TypeMap();

        // Act
        $typeMap->add(new TypeDescriptor('string', false));

        // Assert
        $typeMapAsserts = new TypeMapAsserts($typeMap);

        $typeMapAsserts->assertTypeExists('string', false);
    }

    public function testAddMixed()
    {
        // Arrange
        $typeMap = new TypeMap();

        // Act
        $typeMap->add(new TypeDescriptor('mixed', false));
        $typeMap->add(new TypeDescriptor('mixed', true));

        // Assert
        $typeMapAsserts = new TypeMapAsserts($typeMap);

        $typeMapAsserts
            ->assertTypeExists('mixed', false)
            ->assertTypeExists('mixed', true);
    }

    public function testRemoveExisting()
    {
        // Arrange
        $typeMap = new TypeMap(new TypeDescriptor('string', false));

        // Act
        $typeMap->remove('string', false);

        // Assert
        $typeMapAsserts = new TypeMapAsserts($typeMap);

        $typeMapAsserts->assertTypeNotExists('string');
    }

    public function testIsEqualsWithSameTypes()
    {
        // Arrange
        $mainTypeMap = new TypeMap();
        $anotherTypeMap = new TypeMap();

        // Act
        $mainTypeMap->add(new TypeDescriptor('string', false));
        $anotherTypeMap->add(new TypeDescriptor('string', false));

        // Assert
        $this->assertTrue($mainTypeMap->isEquals($anotherTypeMap));
    }

    public function testIsEqualsWithSameTypesButDifferentOrder()
    {
        // Arrange
        $mainTypeMap = new TypeMap(
            new TypeDescriptor('string', false),
            new TypeDescriptor('int', false)
        );
        $anotherTypeMap = new TypeMap(
            new TypeDescriptor('int', false),
            new TypeDescriptor('string', false),
        );

        // Act
        $isEquals = $mainTypeMap->isEquals($anotherTypeMap);

        // Assert
        $this->assertTrue($isEquals);
    }

    public function testNotIsEqualsWithDifferentTypes()
    {
        // Arrange
        $mainTypeMap = new TypeMap(new TypeDescriptor('string', false));
        $anotherTypeMap = new TypeMap(new TypeDescriptor('int', false));

        // Act
        $isEquals = $mainTypeMap->isEquals($anotherTypeMap);

        // Assert
        $this->assertFalse($isEquals);
    }

    public function testMergeWithSameTypeNameAndIsArray()
    {
        // Arrange
        $mainTypeMap = new TypeMap();
        $anotherTypeMap = new TypeMap();

        // Act
        $mainTypeMap->add(new TypeDescriptor('int', false));
        $anotherTypeMap->add(new TypeDescriptor('int', false));

        $mainTypeMap->merge($anotherTypeMap);

        // Assert
        $typeMapAsserts = new TypeMapAsserts($mainTypeMap);

        $typeMapAsserts->assertTypeExists('int', false);
    }

    public function testMergeWithSameTypeNameAndDifferentIsArray()
    {
        // Arrange
        $mainTypeMap = new TypeMap();
        $anotherTypeMap = new TypeMap();

        // Act
        $mainTypeMap->add(new TypeDescriptor('int', false));
        $anotherTypeMap->add(new TypeDescriptor('int', true));

        $mainTypeMap->merge($anotherTypeMap);

        // Assert
        $typeMapAsserts = new TypeMapAsserts($mainTypeMap);

        $typeMapAsserts
            ->assertTypeExists('int', false)
            ->assertTypeExists('int', true);
    }

    public function testMergeMixedOnly()
    {
        // Arrange
        $mainTypeMap = new TypeMap(new TypeDescriptor('mixed', false));
        $anotherTypeMap = new TypeMap(new TypeDescriptor('mixed', true));

        // Act
        $mainTypeMap->merge($anotherTypeMap);

        // Assert
        $typeMapAsserts = new TypeMapAsserts($mainTypeMap);

        $typeMapAsserts
            ->assertTypeExists('mixed', false)
            ->assertTypeExists('mixed', true);
    }

    public function testMergeMixedNotArrayReplacedByConcreteType()
    {
        // Arrange
        $mainTypeMap = new TypeMap(new TypeDescriptor('mixed', false));
        $anotherTypeMap = new TypeMap(new TypeDescriptor('int', false));

        // Act
        $mainTypeMap->merge($anotherTypeMap);

        // Assert
        $typeMapAsserts = new TypeMapAsserts($mainTypeMap);

        $typeMapAsserts
            ->assertTypeNotExists('mixed')
            ->assertTypeExists('int', false);
    }

    public function testMergeMixedArrayReplacedByConcreteType()
    {
        // Arrange
        $mainTypeMap = new TypeMap(new TypeDescriptor('mixed', true));
        $anotherTypeMap = new TypeMap(new TypeDescriptor('int', true));

        // Act
        $mainTypeMap->merge($anotherTypeMap);

        // Assert
        $typeMapAsserts = new TypeMapAsserts($mainTypeMap);

        $typeMapAsserts
            ->assertTypeNotExists('mixed')
            ->assertTypeExists('int', true);
    }

    public function testMergeCombinedMixedReplacedByConcreteType()
    {
        // Arrange
        $mainTypeMap = new TypeMap(new TypeDescriptor('mixed', false), new TypeDescriptor('mixed', true));
        $anotherTypeMap = new TypeMap(new TypeDescriptor('string', false), new TypeDescriptor('int', true));

        // Act
        $mainTypeMap->merge($anotherTypeMap);

        // Assert
        $typeMapAsserts = new TypeMapAsserts($mainTypeMap);

        $typeMapAsserts
            ->assertTypeNotExists('mixed')
            ->assertTypeExists('int', true)
            ->assertTypeExists('string', false);
    }

    public function testMergeCombinedConcreteDoesNotReplacedByMixed()
    {
        // Arrange
        $mainTypeMap = new TypeMap(new TypeDescriptor('string', false), new TypeDescriptor('int', true));
        $anotherTypeMap = new TypeMap(new TypeDescriptor('mixed', false), new TypeDescriptor('mixed', true));

        // Act
        $mainTypeMap->merge($anotherTypeMap);

        // Assert
        $typeMapAsserts = new TypeMapAsserts($mainTypeMap);

        $typeMapAsserts
            ->assertTypeNotExists('mixed')
            ->assertTypeExists('int', true)
            ->assertTypeExists('string', false);
    }

    public function testToString()
    {
        $typeMap = new TypeMap(
            new TypeDescriptor('string', false),
            new TypeDescriptor('string', true),
            new TypeDescriptor('int', false)
        );

        $this->assertEquals('string, array of string, int', (string)$typeMap);
    }

    public function testConvertToJson()
    {
        $typeMap = new TypeMap(
            new TypeDescriptor('mixed', true),
            new TypeDescriptor('int', false),
        );

        $this->assertEquals(
            '{"types":[{"name":"mixed","isArray":true},{"name":"int","isArray":false}]}',
            json_encode($typeMap)
        );
    }
}